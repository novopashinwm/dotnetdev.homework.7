﻿using System.Collections.Generic;

namespace WebApi.Models
{
    public static class FakeDataFactory
    {
        public static List<Customer> Customers => new List<Customer>() 
        {
            new Customer() 
            {
                Id = 1,
                Firstname = "Иванов",
                Lastname = "Сергей"
            },
            new Customer()
            {
                Id = 2,
                Firstname = "Моргунов",
                Lastname = "Алексей"
            },
            new Customer()
            {
                Id = 3,
                Firstname = "Никулин",
                Lastname = "Юрий"
            }
        };
    }
}
