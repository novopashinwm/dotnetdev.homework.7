﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualBasic;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using WebApi.Abstractions;

namespace WebApi.Repos
{
    public class EFRepository<T>: IRepository<T> where T : BaseEntity
    {
        private readonly DbContext _dataContext;
        public EFRepository(DbContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dataContext.Set<T>().ToListAsync();
        }

        public async Task<T?> GetAsync(long id)
        {
            return await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task AddAsync(T entity)
        {
            var ID = await _dataContext.Set<T>().MaxAsync(x => x.Id + 1);
            entity.Id = ID;
            await _dataContext.Set<T>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();
        }


    }
}
