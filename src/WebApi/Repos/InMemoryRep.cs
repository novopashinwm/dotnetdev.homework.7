﻿using Microsoft.AspNetCore.Http.Features;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using WebApi.Abstractions;

namespace WebApi.Repos
{
    public class InMemoryRep<T> : IRepository<T> where T : BaseEntity
    {
        private List<T> _items;

        public InMemoryRep(List<T> list) 
        {
            _items = list;

        }
        public Task AddAsync(T entity)
        {
            _items.Add(entity);
            return Task.CompletedTask;
        }

        public Task<IEnumerable<T>> GetAllAsync() => Task.FromResult<IEnumerable<T>>(_items);

        public Task<T> GetAsync(long id)
        {
            return Task.FromResult(_items.Where(x => x.Id == id).FirstOrDefault());
        }
    }
}
