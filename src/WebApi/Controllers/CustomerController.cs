using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic;

using WebApi.Abstractions;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;

        public  CustomerController(IRepository<Customer> customerRepository) 
        {
            _customerRepository = customerRepository;
        }

        [HttpGet("{id:long}")]   
        public async Task<IActionResult> GetCustomerAsync([FromRoute] long id)
        {
            var res = await _customerRepository.GetAsync(id);
            if (res != null)
                return Ok(res);
            return NotFound($"Customer id={id} not found!!!");
        }

        [HttpPost("")]   
        public async Task<IActionResult> CreateCustomerAsync([FromBody] Customer customer)
        {
            if (await _customerRepository.GetAsync(customer.Id) != null)
                return Conflict($"An existing customer with the id={customer.Id} was already found.");

            await _customerRepository.AddAsync(customer);
            return Ok($"Client id={customer.Id} added success!!");
        }

        [HttpGet]
        public async Task<IEnumerable<Customer>> GetAll()
        {

            return await _customerRepository.GetAllAsync();
        }
    }
}