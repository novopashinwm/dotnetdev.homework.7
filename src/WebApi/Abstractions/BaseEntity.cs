﻿
namespace WebApi.Abstractions
{
    public class BaseEntity
    {
        public long Id { get; set; }
    }
}
