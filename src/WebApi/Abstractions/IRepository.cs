﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Abstractions
{
    public interface IRepository<T> 
    {
        Task<T?> GetAsync(long id);
        Task AddAsync(T entity);

        Task<IEnumerable<T>> GetAllAsync();

    }
}
