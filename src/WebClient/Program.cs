﻿using System;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using System.Text.Json;

namespace WebClient
{
    static class Program
    {
        const string server_addr = "http://localhost:5000/";
        private static readonly HttpClient http_client = new HttpClient() 
        {
            BaseAddress = new Uri(server_addr)
        };

        enum Mode 
        {
            None,
            GET_CLIET,
            ADD_CLIENT,
        }

        static Task Main(string[] args)
        {
            while (true) 
            {
                Mode mode = EnterMode();
                switch (mode)
                {
                    case Mode.GET_CLIET : GetClient();  break;
                    case Mode.ADD_CLIENT: AddClient(); break;
                }
                Console.WriteLine(new string('+',50));
            }
            
        }

        private static void AddClient()
        {
            var request = RandomCustomer();
            Console.Write("Введите id нового клиента: ");
            int.TryParse(Console.ReadLine(), out int id);
            request.Id = id;

            using StringContent jsonContent = new(
                
                JsonSerializer.Serialize(request),
                Encoding.UTF8,
                "application/json");

            using HttpResponseMessage response = http_client.PostAsync(
                "customers", jsonContent).Result;

            var jsonResponse = response.Content.ReadAsStringAsync().Result;

            Console.WriteLine($"Code: {response.StatusCode}");
        }

        private static Customer GetCustomerById(int id)
        {
            using var client = new HttpClient();
            var result =  client.GetAsync(Program.server_addr + $"customers/{id}").Result;

            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var res = result.Content.ReadAsStringAsync().Result;                
                var customer = Newtonsoft.Json.JsonConvert.DeserializeObject<Customer>(res);
                return customer;
            }

            return null;
        }

        private static void GetClient()
        {
            Console.Write("Введите id искомого клиента: ");
            int.TryParse(Console.ReadLine(), out int id);
            var client = GetCustomerById(id);
            if (client != null)
            {
                Console.WriteLine(client);
            }
            else 
            {
                Console.WriteLine($"Клиент с {id} не найден !!!");
            }
        }

        private static Mode EnterMode()
        {
            Mode ret = Mode.None;
            int mode = 0;
            while (mode < 1 || mode > 2) 
            {
                Console.WriteLine("Выберите режим работы:");
                Console.WriteLine("1 - Получение данных по клиенту.");
                Console.WriteLine("2 - Создание нового клиента");
                int.TryParse(Console.ReadLine(), out mode);
                if (mode < 1 || mode > 2)
                {
                    Console.WriteLine("Выбран не корректный режим работы.");
                    Console.WriteLine("Повторите ввод.");
                }
                else if (mode == 1)
                    ret = Mode.GET_CLIET;
                else if (mode == 2)
                    ret = Mode.ADD_CLIENT;
                Console.WriteLine();
                Console.WriteLine(new string('=',50));
            }

            return ret;
        }


        private static CustomerCreateRequest RandomCustomer()
        {
            const int size = 7;
            string[] firstNames = new string[size] { "Алексей", "Владимир", "Виталий", "Павел", "Сергей" , "Митрофан", "Степан" };
            string[] lastNames = new string[size] { "Алексеев", "Путин", "Наливкин", "Трофимов", "Дроздовский", "Парфенов","Студеникин" };

            Random r = new Random();

            var customerRequest = new CustomerCreateRequest(
            firstNames[r.Next(0, size)],
            lastNames[r.Next(0, size)]
            );

            return customerRequest;
        }
    }
}